//
//  ViewController.swift
//  Lesson3
//
//  Created by Grisha Stetsenko on 21.08.2018.
//  Copyright © 2018 Grisha Stetsenko. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        reverseOurNumber(64)
        
    }
    
    func reverseOurNumber(_ number: Int) {
        print("We had number: \(number)")
        let reversedFirst = number%10
        let reversedSecond = (number/10)%10
        print("Our reversed number is: \(reversedFirst)\(reversedSecond)")
    }
}



